Documentation
=============

The project directory is divided into 3 folders i.e. actual-code folder (named as 'math'), testing folder (test) and documentation folder (doc). 

.. note::
    
    This section is included to demonstrate the proper directory-structure for the project. Please clone the repository for better understanding of the directory-structure along with code sharing between these directories. 

    .. code-block:: shell

        $ git clone https://pythondsp@bitbucket.org/pythondsp/pytestguide.git


Modules
-------

Python
^^^^^^

.. automodule:: mathEx


Cython
^^^^^^

.. automodule:: mathCy  

Tests
-----

.. automodule:: test_mathEx